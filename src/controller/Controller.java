package controller;

import api.ISTSManager;

import model.data_structures.IList;
import model.logic.Route;
import model.logic.STSManager;
import model.logic.Stop;

public class Controller {

	/**
	 * Reference to the routes and stops manager
	 */
	private static ISTSManager  manager = new STSManager();
	
	public static void loadRoutes() 
	{
		manager.loadRoutes("data/routes.txt");
	}
		
	public static void loadTrips() {
		manager.loadTrips("data/trips.txt");
	}

	public static void loadStopTimes() {
		manager.loadStopTimes("data/stop_times.txt");
	}
	
	public static void loadStops() {
		manager.loadStops("data/stops.txt");
	}
	
	public static IList<Route> routeAtStop(String stopName) {
		return manager.routeAtStop(stopName);
	}
	
	public static IList<Stop> stopsRoute(String routeName, String direction) {
		return null;
	}
}
