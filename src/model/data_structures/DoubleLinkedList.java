package model.data_structures;

import java.util.Iterator;


public class DoubleLinkedList < T > implements IList< T >
{
	Node<T> first;

	Node<T> last;

	Node<T> current;

	public DoubleLinkedList()
	{
		first = null;
		last = null;
		current = null;
	}
	public void delete() throws Exception
	{
		if( first == null )
		{
			throw new Exception("The list is empty");
		}
		else
		{
			if( first == current && current == last)
			{
				first = null;
				current = null;
				last = null;
			}
			else if(current == first){
				current = first.getNext();
				first = current;
			}
			else if(current == last )
			{
				current = last.getPrevious();
				current.changeNext(null);
				last = current;
			}
			else
			{
				Node<T> Ncurrent = current.getNext();
				current.getPrevious().changeNext(Ncurrent);
				current = Ncurrent;
			}
		}
	}
	public void add( T elem ) 
	{
		if( elem ==  null) throw new NullPointerException();
		if( first == null )
		{
			first = new Node<T>(elem);
			last = first;
			current = first;
		}
		else
		{
			Node<T> node = new Node<T>(elem);

			if(getSize() == 1){
				last = node;
			}

			node.changeNext(current.getNext());
			current.changeNext(node);
			node.changePrevious(current);

			//current = node;
		}
	}
	public void addAtK(int pos, T elem) 
	{
		Node<T> add = new Node<T>(elem);
		int size = getSize();

		if(pos > size || pos < 0) throw new IndexOutOfBoundsException();
		if(elem == null ) throw new NullPointerException();
		if( size == 0 )
		{
			first = new Node<T>(elem);
			last = first;
			current = first;
		}
		else if( size == 1)
		{
			first = new Node<T>(elem);
			first.changeNext(last);
			last.changePrevious(first);
		}
		else if(pos == 0) 
		{ 
			add.changeNext(first);
			first.changePrevious(add);
			first = add;
		}
		else if( pos == size )
		{
			add.changePrevious(last);
			last.changeNext(add);
			last = add;
		}
		else 
		{			
			Node<T> temp = first;
			int position = 0;
			boolean added = false;
			while( !added )
			{
				if(position == pos - 1)
				{
					add.changeNext(temp.getNext());
					temp.changeNext(add);
					add.changePrevious(temp);
					added = true;
				}
				temp = temp.getNext();
				position++;
			}
		}
	}

	public void deleteAtK(int pos) 
	{
		// TODO Auto-generated method stub
		int size = getSize();
		if(pos > size - 1 || pos < 0) throw new IndexOutOfBoundsException();
		if(size == 1)
		{
			first = null;
			current = null;
			last = null;
		}
		else if(pos == 0)
		{
			if(current.equals(first))
			{
				current = first.getNext();
			}
			if( first.equals(last) )
			{
				first = first.getNext();
				last = first;
			}
			else {first = first.getNext();}

		}
		else if(pos == size - 1)
		{
			if(current == last){
				current = last.getPrevious();
			}

			last.getPrevious().changeNext(null);
			last = last.getPrevious();

		}
		else
		{
			int position = 1;
			boolean delete = false;
			Node<T> temp = first.getNext();
			while(!delete)
			{
				if( position == pos )
				{
					if(current.getElement().equals(getElementAtK(pos)))
					{
						current = current.getPrevious();
					}
					temp.getPrevious().changeNext(temp.getNext());
					temp.getNext().changePrevious(temp.getPrevious());
					delete = true;
				}
				position++;
				temp = temp.getNext();
			}
		}
	}

	public Integer getSize() 
	{
		Integer size = 0;
		Node<T> nodo = first;

		while( nodo != null)
		{
			size++;
			nodo = nodo.getNext( );
		}
		return size;
	}

	public T getFirst()
	{
		return first.getElement();
	}

	public T getLast()
	{
		return last.getElement();
	}

	public Iterator<T> iterator() 
	{
		return new Iterator<T>() {

			@Override
			public boolean hasNext() 
			{
				// TODO Auto-generated method stub
				if(current == null) System.out.println("Is null current");
				return current.getNext() != null? true: false;
			}

			@Override
			public T next() {
				// TODO Auto-generated method stub
				if( current.getNext() == null ) 
				{
					System.out.println("Is null");
					throw new NullPointerException();
				}

				current = current.getNext();
				return getElement();
			}
		};
	}


	public void previous()
	{
		if(current.equals(first))throw new NullPointerException();
		else if((first == null) && (last == null) && (current == null)) throw new NullPointerException();
		else current = current.getPrevious();	
	}

	@Override
	public void addAtEnd(T elem) {
		// TODO Auto-generated method stub
		Node<T> lastest = new Node<T>(elem);
		if(elem == null) throw new NullPointerException();
		else if(first == null) {
			first = new Node<T>(elem);
			last = first;
			current = first;
		}
		else
		{
			last.changeNext(lastest);
			lastest.changePrevious(last);
			last = lastest;
		}

	}

	@Override
	public T getElement() {
		// TODO Auto-generated method stub
		return current.getElement();
	}

	@Override
	public T getElementAtK(int pos) {
		// TODO Auto-generated method stub
		T search = null;
		int size = getSize();
		if(pos > size|| pos < 0) throw new IndexOutOfBoundsException();
		//else if(first == null) throw new NullPointerException();
		else if(pos == 0) search = first.getElement();
		//last -> es el primero
		else if(pos == size - 1) search  = last.getElement();
		else{
			Node<T> temp = first.getNext();
			boolean found = false;
			int i = 1;
			while(!found){
				if(i == pos){
					search = temp.getElement();
					found = true;
				}
				temp = temp.getNext();
				i++;
			}
		}
		return search;
	}
	@Override
	public void next() {
		// TODO Auto-generated method stub
		if((first == null) && (last == null) && (current == null)) throw new NullPointerException();
		else current = current.getNext();
	}

	public void sort(int beg, int end){
		if(first == null || getSize() == 0) return;
		if(beg >= end) return;
		
		int pivot = beg +(end - beg)/2;
		T pM = getElementAtK(pivot); 
		int i = beg, j = end;
		while(i <= j){
			while(getElementAtK(i).toString().compareTo(pM.toString()) < 0){
				i++;
			}
			while(getElementAtK(j).toString().compareTo(pM.toString()) > 0){
				j--;
			}
			if(i <= j){
				T temp = getElementAtK(i);
				T elemJ = getElementAtK(j);
				set(i, elemJ);
				set(j, temp);
				i++;
				j--;
			}
			if(beg < j){
				sort(beg, j);
			}
			if(end > i){
				sort(i, end);
			}
		}
	}
    public T set( int pos, T elem )throws IndexOutOfBoundsException
    {
    	Node<T> var = first;
    	T chg = null;
    	int i = 0;
    	if(i == pos){
    		first.changeElement(elem);
    	}
    	else{
	    	while(i < pos){
	    		var = first.getNext();
	    		i++;
	    	}
	    	if(i == pos){
	    		chg = var.getElement();
	    		var.changeElement(elem);
	    	}
    	}
		return chg;
    }
	//	public void deleteG(T elem) throws Exception
	//	{
	//		if(elem == null) throw new NullPointerException();
	//		if( first == null )
	//		{
	//			throw new Exception("The list is empty");
	//		}
	//		else
	//		{
	//			if( elem.equals(first.getElement()))
	//			{
	//				first = first.getNext();
	//				current = first;
	//			}
	//			else if( elem.equals(last.getElement()))
	//			{
	//				last = last.getPrevious();
	//				last.changeNext(null);
	//			}
	//			else if( elem.equals(first.getElement()) && first.equals(last))
	//			{
	//				first = null;
	//				current = null;
	//				last = null;
	//			}
	//			else
	//			{
	//				boolean delete = false;
	//				Node<T> temp = first.getNext();
	//				while(!delete && temp != null)
	//				{
	//					if(temp.getElement().equals(elem))
	//					{
	//						temp.getPrevious().changeNext(temp.getNext());
	//						temp.getNext().changePrevious(temp.getPrevious());
	//						delete = true;
	//					}
	//					temp = temp.getNext();
	//				}
	//				if(!delete) throw new Exception( elem.toString() + " doesn�t exist in this list");
	//			}
	//		}
	//	}
	//
	public Node<T> getCurrent()
	{
		return current;
	}
	public Node<T> getFirstNode()
	{
		return first;
	}
	public Node<T> getLastNode()
	{
		return last;
	}
	public void restart()
	{
		current = first;
	}
}

