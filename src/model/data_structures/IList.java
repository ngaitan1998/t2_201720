package model.data_structures;

/**
 * Abstract Data Type for a list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface IList<T> extends Iterable<T> 
{
	void delete( ) throws Exception;
	
	void add( T elem );

	void addAtK( int pos, T elem );
	
	void addAtEnd(T elem);

	T getElement();
	
	T getElementAtK(int pos);

	void deleteAtK(int pos);
	
	Integer getSize( );
	
	void next();
	
	void previous();
	
	void restart();
	
}
