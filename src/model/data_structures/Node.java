package model.data_structures;

public class Node < T > 
{
	private Node<T> previous;
	
	private Node<T> next;
	
	private T elem;
	
	public Node( T pElem )
	{
		elem = pElem;
		previous = null;
		next = null;
	}
	
	public int id()
	{
		return elem.hashCode();
	}
	public Node<T> getNext()
	{
		return next;
	}
	public Node<T> getPrevious()
	{
		return previous;
	}
	public T getElement()
	{
		return elem;
	}
	public void changeNext( Node<T> pNext )
	{
		next = pNext;
	}
	public void changePrevious( Node<T> pPrevious )
	{
		previous = pPrevious;
	}
	public void changeElement(T nElem){
		elem = nElem;
	}
}
