package model.logic;

import java.awt.Color;
import java.net.URL;

public class Route 
{	
	private int id;
	private String agencyId;
	private String routeNum;
	private String routeName;
	private String routeDesc;
	private String routeUrl;
	private Color routeColor;
	private Color routeTextColor;

	public Route( int pId, String pAngencyId, String pRouteNum, String pRouteName, String pRouteDesc, String pUrl, Color pRouteColor, Color pRouteText)
	{
		id = pId;
		agencyId = pAngencyId;
		routeDesc = pRouteDesc;
		routeColor = pRouteColor;
		routeTextColor = pRouteText;
		routeUrl = pUrl;
		routeNum = pRouteNum;
		routeName = pRouteName;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(String agencyId) {
		this.agencyId = agencyId;
	}

	public String getRouteNum() {
		return routeNum;
	}

	public void setRouteNum(String routeNum) {
		this.routeNum = routeNum;
	}

	public String getRouteName() {
		return routeName;
	}

	public void setRouteName(String routeName) {
		this.routeName = routeName;
	}

	public String getRouteDesc() {
		return routeDesc;
	}

	public void setRouteDesc(String routeDesc) {
		this.routeDesc = routeDesc;
	}

	public String getRouteUrl() {
		return routeUrl;
	}

	public void setRouteUrl(String routeUrl) {
		this.routeUrl = routeUrl;
	}

	public Color getRouteColor() {
		return routeColor;
	}

	public void setRouteColor(Color routeColor) {
		this.routeColor = routeColor;
	}

	public Color getRouteTextColor() {
		return routeTextColor;
	}

	public void setRouteTextColor(Color routeTextColor) {
		this.routeTextColor = routeTextColor;
	}
	public String toString()
	{
		return id + "";
	}
}

