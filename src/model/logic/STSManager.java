package model.logic;

import java.io.BufferedReader;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.sql.Time;
import java.util.Calendar;
import java.util.Iterator;

import api.ISTSManager;
import model.data_structures.DoubleLinkedList;
import model.data_structures.IList;
import model.data_structures.RingList;

public class STSManager implements ISTSManager 
{
	IList<Route> routes = new DoubleLinkedList<Route>();
	IList<Stop> stops = new RingList<Stop>();
	IList<StopTime> stopTimes = new DoubleLinkedList<StopTime>();
	IList<Trip> trips = new RingList<Trip>();

	@Override
	public void loadRoutes(String routesFile)
	{

		try {
			BufferedReader reader = new BufferedReader(new FileReader(new File(routesFile)));
			reader.readLine();
			String line = reader.readLine();
			while( line != null )
			{
				String data[] = line.split(",");
				int id = Integer.parseInt(data[0]);
				String agencyId = data[1];
				String routeNum = data[2];
				String routeName = data[3];
				String routeDesc = data[4];
				String routeUrl = data[5];
				routes.add(new Route(id, agencyId, routeNum, routeName, routeDesc, routeUrl, null, null));
				line = reader.readLine();
			}
			reader.close();
			
		} catch (Exception e) {
			// TODO: handle exception
		}		
	}

	@Override
	public void loadTrips(String tripsFile) 
	{
		// TODO Auto-generated method stub

		try {
			BufferedReader reader = new BufferedReader(new FileReader(new File(tripsFile)));
			reader.readLine();
			String line = reader.readLine();
			while( line != null )
			{
				String data[] = line.split(",");
				int routeId = Integer.parseInt(data[0]);
				int serviceId = Integer.parseInt(data[1]);
				String tripId = data[2];
				String headSing = data[3];
				String tripShortName = data[4];
				String directionId = data[3];
				String blockId = data[4];
				String shapeId = data[5];
				int bikeAllowed = Integer.parseInt(data[6]);
				int wheelchairFriendly = Integer.parseInt(data[7]);

				trips.addAtK(0 ,new Trip(routeId, serviceId, tripId, headSing, 
						tripShortName, directionId, blockId, shapeId, bikeAllowed, wheelchairFriendly));

				line = reader.readLine();
				
			}
			reader.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}		
	}

	@Override
	public void loadStopTimes(String stopTimesFile)
	{
		// TODO Auto-generated method stub
		try {
			BufferedReader reader = new BufferedReader(new FileReader(new File(stopTimesFile)));
			reader.readLine();
			String line = reader.readLine();
			while( line != null )
			{

				String data[] = line.split(",");

				String id = (data[0]).toString();

				String arrivalTimeData = data[1];
				//Time arrival = new Time(Integer.parseInt(arrivalTimeData[0]), Integer.parseInt(arrivalTimeData[1]), Integer.parseInt(arrivalTimeData[2]));

				String departureTimeData = data[2];
		
				int stopId = Integer.parseInt(data[3]);

				String stopSequence = data[4];

				String stopSign = data[5];

				int pickUp = Integer.parseInt(data[6]);

				int drop = Integer.parseInt(data[7]);

				double distance;
				if(data.length > 8) 
				{
					distance = Double.parseDouble(data[8]);
				}
				else distance = 0;

				stopTimes.addAtEnd(new StopTime(id, arrivalTimeData, departureTimeData, stopId, stopSequence, stopSign, pickUp, drop, distance));

				line = reader.readLine();
			}
			reader.close();
		} catch (IOException e) {

		}
	}

	@Override
	public void loadStops(String stopsFile) 
	{
		try {
			BufferedReader reader = new BufferedReader(new FileReader(new File(stopsFile)));
			reader.readLine();
			String line = reader.readLine();
			while( line != null )
			{
				String data[] = line.split(",");

				int id = Integer.parseInt(data[0]);
				int code;
				if(data[1] != null && !data[1].equals(" ") && !data[1].isEmpty())
				{					
					code= Integer.parseInt(data[1]);
				}
				else code = 0;
				String name = data[2];
				String stopDesc = data[3];
				double stopLat = Double.parseDouble(data[4]);
				double stopLon = Double.parseDouble(data[5]);
				String stopZone = data[6];
				int locationType = Integer.parseInt(data[8]);

				stops.add(new Stop(id, code, name, stopDesc, stopLat, stopLon, stopZone, null, locationType, null));
				line = reader.readLine();
			}
			reader.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();

		}
	}

	@Override
	public IList<Route> routeAtStop(String stopName)
	{
		// TODO Auto-generated method stub
		IList<Route> names = new DoubleLinkedList<Route>();

		//Encuentra el id
		Iterator<Stop> iter = stops.iterator();
		boolean stop = false;
		Stop current = iter.next();
		int id = 0;
		while(iter.hasNext() && !stop)
		{
			if(current.getName().equals(stopName) || current.getName().contains(stopName))
			{
				id = current.getId();
				stop = true;
			}
			current = iter.next();
		}
		//Busca la parada
		DoubleLinkedList<String> listaIDs = new DoubleLinkedList<String>();
		if(id != 0)
		{
			Iterator<StopTime> stopsT = stopTimes.iterator();
			StopTime stp = stopsT.next();
			int cont = 1;
			stop = false;
			int size = stopTimes.getSize() - 1;
			while(iter.hasNext() && !stop)
			{	
				if(stp.getStopId() == id)
				{
					listaIDs.addAtEnd(stp.getTripId());
				}
				if(cont == size - 1)
				{
					stop = true;
				}
				stp = stopsT.next();
				cont++;
			}
		}
		//Encuentra los viajes con esa parada
		DoubleLinkedList<Integer> listaIDRoutes = new DoubleLinkedList<Integer>();
		if(listaIDs.getElementAtK(0) != null)
		{
			Iterator<Trip> tripIter = trips.iterator();
			Trip actual = tripIter.next();
			int cont = 1;
			stop = false;
			int size = trips.getSize();
			while(iter.hasNext() && !stop)
			{
				Iterator<String> temp = listaIDs.iterator();
				String idTrip = temp.next();
				boolean end = false;
				while(temp.hasNext() && !end)
				{
					if((idTrip + "").equals(actual.getTripId()))
					{
						end = true;
						listaIDRoutes.addAtEnd(actual.getRouteId());
					}
					idTrip = temp.next();
				}
				actual = tripIter.next();
				listaIDs.restart();
				cont++;
				if(cont == size) stop = true;
			}
		}
		//Encuentra las rutas de los viajes
		if(listaIDRoutes.getElementAtK(0) != null)
		{
			routes.restart();
			Iterator<Route> routeIter = routes.iterator();
			Route actual;
			while(iter.hasNext())
			{
				System.out.println("entra");
				actual = routeIter.next();
				Iterator<Integer> temp = listaIDRoutes.iterator();
				int idRoutes;
				boolean end = false;
				while(temp.hasNext() && !end)
				{
					System.out.println("loop");
					idRoutes = temp.next();
					if((idRoutes + "").equals(actual.getId()))
					{
						end = true;
						names.addAtEnd(actual);
					}
				}
				listaIDRoutes.restart();
				System.out.println("sale");
			}
		}
		return names;

	}

	@Override
	public IList<Stop> stopsRoute(String routeName, String direction)
	{
		// TODO Auto-generated method stub
		IList<Stop> searchStops = new DoubleLinkedList<Stop>();
		IList<Route> routesWhitTrips = new DoubleLinkedList<Route>();
		IList<Trip> tripsWhitStops = new DoubleLinkedList<Trip>();
		IList<StopTime> StpTimesWithStop = new DoubleLinkedList<StopTime>();
		
		Iterator<Route> iterA = routes.iterator();
		while(iterA.hasNext()){
			Route rt = iterA.next();
			if(rt.getRouteName().equals(routeName)){
				routesWhitTrips.add(rt);
			}
		}
		
		for(Route rta: routesWhitTrips){
			Iterator<Trip> iterB = trips.iterator();
			while(iterB.hasNext()){
				Trip tp = iterB.next();
				if(rta.getId() == tp.getRouteId()){
					tripsWhitStops.add(tp);
				}
			}
		}
		
		for(Trip tp: tripsWhitStops){
			Iterator<StopTime> iterC = stopTimes.iterator();
			while(iterC.hasNext()){
				StopTime stpTm = iterC.next();
				if(stpTm.getTripId()== tp.getTripId()){
					StpTimesWithStop.add(stpTm);
				}
			}
		}
		
		for(StopTime stpT : StpTimesWithStop){
			Iterator<Stop> iterD = stops.iterator();
			while(iterD.hasNext()){
				Stop stp = iterD.next();
				if(stp.getId() == stpT.getStopId()){
					searchStops.add(stp);
				}
			}
		}
		

		return searchStops;
	}

}
