package model.logic;

import java.net.URL;

public class Stop 
{
	private int id;
	private int code;
	private String name;
	private String stopDesc;
	private double stopLat;
	private double stopLon;
	private String stopZone;
	private URL stopUrl;
	private int locationType;
	private String parentStation;
	
	public Stop(int pId, int pCode, String pName, String pStopDesc, double pStopLat, double pStopLon, String pStopZone, URL pUrl, int pLocType, String pParent)
	{
		id = pId;
		code = pCode;
		name = pName;
		stopDesc = pStopDesc;
		stopLat = pStopLat;
		stopLon = pStopLon;
		stopZone = pStopZone;
		stopUrl = pUrl;
		locationType = pLocType;
		parentStation = pParent;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStopDesc() {
		return stopDesc;
	}

	public void setStopDesc(String stopDesc) {
		this.stopDesc = stopDesc;
	}

	public double getStopLat() {
		return stopLat;
	}

	public void setStopLat(double stopLat) {
		this.stopLat = stopLat;
	}

	public double getStopLon() {
		return stopLon;
	}

	public void setStopLon(double stopLon) {
		this.stopLon = stopLon;
	}

	public String getStopZone() {
		return stopZone;
	}

	public void setStopZone(String stopZone) {
		this.stopZone = stopZone;
	}

	public URL getStopUrl() {
		return stopUrl;
	}

	public void setStopUrl(URL stopUrl) {
		this.stopUrl = stopUrl;
	}

	public int getLocationType() {
		return locationType;
	}

	public void setLocationType(int locationType) {
		this.locationType = locationType;
	}

	public String getParentStation() {
		return parentStation;
	}

	public void setParentStation(String parentStation) {
		this.parentStation = parentStation;
	}
	public String toString()
	{
		return id + "";
	}

}
