package model.logic;

public class StopTime 
{
	private String tripId;
	private String arrivalTime;
	private String departureTime;
	private int stopId;
	private String stopSequence;
	private String stopHeadSign;
	private int pickUpType;
	private int dropType;
	private double distanceTraveled;
	
	public StopTime(String pId, String pArrivalTime, String pDepartureTime, int pStopId, String pStopSequence, 
			String pStopSign, int pPickupType, int pDropType, double pDistance )
	{
		tripId = pId;
		arrivalTime = pArrivalTime;
		departureTime = pDepartureTime;
		stopId = pStopId;
		stopSequence = pStopSequence;
		stopHeadSign = pStopSign;
		pickUpType = pPickupType;
		dropType = pDropType;
		distanceTraveled = pDistance;
	}

	public String getTripId() {
		return tripId;
	}

	public void setTripId(String tripId) {
		this.tripId = tripId;
	}

	public String getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public String getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}

	public int getStopId() {
		return stopId;
	}

	public void setStopId(int stopId) {
		this.stopId = stopId;
	}

	public String getStopSequence() {
		return stopSequence;
	}

	public void setStopSequence(String stopSequence) {
		this.stopSequence = stopSequence;
	}

	public String getStopHeadSign() {
		return stopHeadSign;
	}

	public void setStopHeadSign(String stopHeadSign) {
		this.stopHeadSign = stopHeadSign;
	}

	public int getPickUpType() {
		return pickUpType;
	}

	public void setPickUpType(int pickUpType) {
		this.pickUpType = pickUpType;
	}

	public int getDropType() {
		return dropType;
	}

	public void setDropType(int dropType) {
		this.dropType = dropType;
	}

	public double getDistanceTraveled() {
		return distanceTraveled;
	}

	public void setDistanceTraveled(double distanceTraveled) {
		this.distanceTraveled = distanceTraveled;
	}
	public String toString()
	{
		return "" + stopId;
	}
}
