package view;

import java.util.Scanner;


import controller.Controller;
import model.data_structures.IList;
import model.logic.Route;
import model.logic.Stop;

public class STSManagerView {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin){
			printMenu();
			
			int option = sc.nextInt();
			
			switch(option){
				case 1:
					Controller.loadRoutes();
					break;
				case 2:
					Controller.loadTrips();
					break;
				case 3:
					Controller.loadStopTimes();
					break;
				case 4:
					Controller.loadStops();
					break;
				case 5:
					System.out.println("Cargando los datos por favor espere... ");
					Controller.loadRoutes();
					Controller.loadStops();
					Controller.loadTrips();
					Controller.loadStopTimes();
					System.out.println("Los datos se han cargado exitosamente!");
					break;
				case 6:
					System.out.println("Ingrese el nombre de la parada:");
					String stopName = sc.next();
					IList<Route> routesList = Controller.routeAtStop(stopName);
					System.out.println("Se encontraron "+ routesList.getSize() + " elementos");
					for (Route route : routesList) {
						System.out.println(route.getRouteName());
					}
					break;
				case 7:
					System.out.println("Ingrese el nombre de la ruta");
					String routeName = sc.next();
					System.out.println("Ingrese la dirección de la ruta");
					String direction = sc.next();
					IList<Stop> stopsList = Controller.stopsRoute(routeName, direction);
					System.out.println("Se encontraron " + stopsList.getSize() + " elementos");
					for (Stop stop : stopsList) {
						System.out.println(stop.getName() + " " + stop.getId());
					}
					break;
				case 8:	
					fin=true;
					sc.close();
					break;
			}
		}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 2----------------------");
		System.out.println("1. Cree una nueva coleccion de rutas (data/routes.txt)");
		System.out.println("2. Cree una nueva coleccion de viajes (data/trips.txt)");
		System.out.println("3. Cree una nueva coleccion de rutas reales (data/shapes.txt)");
		System.out.println("4. Cree una nueva coleccion de paradas (data/stops.txt)");
		System.out.println("5. Cargar todas las listas (Pilas con la RAM) ");
		System.out.println("6. Dar rutas para una parada");
		System.out.println("7. Dar paradas de una ruta");
		System.out.println("8. Salir");
		System.out.println("Type the option number for the task, then press enter: (e.g., 1):");
		
	}
}
