package data_structures.test;

import java.util.Iterator;

import junit.framework.TestCase;
import model.data_structures.DoubleLinkedList;

public class DoubleLinkedListTest extends TestCase{

	private DoubleLinkedList<Integer> numeros;


	public void setUp(){
		numeros = new DoubleLinkedList<Integer>();
		try{
			numeros.add(1);
			numeros.add(2);
			numeros.add(3);
			numeros.add(4);
			numeros.add(5);
			numeros.add(6);
			numeros.add(7);
			numeros.add(8);
		}
		catch(Exception e){
			e.printStackTrace();

		}
	}

	public void setUp2(){
		numeros = new DoubleLinkedList<Integer>();
		try{
			numeros.add(1);
		}
		catch(Exception e){
			e.printStackTrace();
		}

	}

	public void setUp3(){
		numeros = new DoubleLinkedList<Integer>();
		try{
		}
		catch(Exception e){
			e.printStackTrace();
		}

	}


	public void testGetFirst(){
		setUp();
		assertEquals("El primer numero no es el esperado",(Integer) 1, numeros.getFirst());
		setUp2();
		assertEquals("El primer numero no es el esperado",(Integer) 1, numeros.getFirst());

	}
	public void testGetLast(){
		setUp();
		assertEquals("El ultimo numero no es el esperado", (Integer) 2, numeros.getLast());
	}
	public void testSize(){
		setUp();
		assertEquals("El tama�o no corresponde a la lista", (Integer) 8, numeros.getSize());
		
		setUp3();
		assertEquals("El tama�o no corresponde a la lista", (Integer) 0, numeros.getSize());
	}
	public void testAdd(){
		setUp();
		numeros.add(9);
		assertEquals("No se agreg� el numero correctamente", (Integer) 9, numeros.getElementAtK(1));
		assertEquals((Integer) numeros.getElement(), numeros.getCurrent().getNext().getPrevious().getElement());
		setUp3();
		numeros.add(1);
		assertEquals("No se agreg� el numero correctamente", (Integer) 1, numeros.getElementAtK(0));
	}
	public void testGet(){
		assertEquals("El elemento actual deber�a ser el primero", (Integer) 1, numeros.getElement());
	}

	public void testNext(){
		setUp();
		//CASE 1: is not the last
		try {
			numeros.next();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			//WONT THROW ANY EXCEPTION
			e1.printStackTrace();
		}
		assertEquals("El elemento actual no corresponde con el esperado", (Integer) 8, numeros.getElement());

		//CASE 2: is the last
		try{
			numeros.next();
		}
		catch(Exception e){

		}

		//CASE 3: empty list
		setUp2();
		try{
			numeros.next();
		}
		catch(Exception e){

		}
	}
	public void testPrevious(){
		setUp();
		//CASE 1: is not the first
		try {
			numeros.next();
			numeros.previous();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			//WONT THROW ANY EXCEPTION
			e1.printStackTrace();
		}
		assertEquals("El elemento actual no corresponde con el esperado", (Integer) 1, numeros.getElement());

		//CASE 2: is the first
		try{
			numeros.previous();
		}
		catch(Exception e){

		}

		//CASE 3: empty list
		setUp2();
		try{
			numeros.previous();
		}
		catch(Exception e){

		}
	}
	public void testGetAtK(){
		setUp();
		assertEquals("El elemento retornado deber�a ser el primero", (Integer) 1, numeros.getElementAtK(0));
		assertEquals("El elemento retornado deber�a ser el ultimo", (Integer) 2, numeros.getElementAtK(numeros.getSize() - 1));
		assertEquals("El elemento no es el esperado", (Integer) 8, numeros.getElementAtK(1));
		try{
			assertEquals("El numero ingresado supera el rango de tama�o de la lista", null, numeros.getElementAtK(123));
		}
		catch (Exception e){

		}

		setUp3();
		try{
			assertEquals("La lista est� vac�a, no deber�a retornar nada", null, numeros.getElementAtK(1));
		}
		catch (Exception e){

		}
	}

	public void addAtEndTest(){
		setUp();
		numeros.addAtEnd(9);
		assertEquals("El ultimo elemento no corresponde con el esperado", (Integer) 9, numeros.getLast());

		setUp3();
		numeros.addAtEnd(1);
		assertEquals("El primer elemento deber�a tambi�n ser el �ltimo", (Integer)1, numeros.getFirst());

	}

	public void testAddAtK(){
		//mas de 1 elemento
		setUp();
		// Al final
		numeros.addAtK(numeros.getSize(), 99);
		assertEquals("El elemento no es el esperado", (Integer) 99, numeros.getLast());

		// Al inicio
		numeros.addAtK(0, 98);
		assertEquals("El elemento no es el esperado", (Integer) 98, numeros.getFirst());

		// En la mitad

		numeros.addAtK(1, 97);

		assertEquals("El elemento no es el esperado", (Integer) 97, numeros.getElementAtK(1));


		//1 Elemento
		setUp2();
		numeros.addAtK( 0, 9 );
		assertEquals("El elemento no es el esperado", (Integer) 9, numeros.getElementAtK( 0 ));

		//0 Elementos
		setUp3();
		numeros.addAtK(0, 9);
		System.out.print(numeros.getFirst());
		assertEquals("El elemento no es el esperado", (Integer) 9, numeros.getFirst());

		try {numeros.addAtK(33, 9); fail("No deberia mandar excepcion");} catch(Exception e) {}

	}
	public void testDeleteAtK() throws Exception{
		setUp(); 

		//CASE 1: the first
		numeros.deleteAtK(0);
		assertEquals("El primer elemento no corresponde con lo esperado", (Integer) 8, numeros.getFirst());
		assertEquals("El elemento actual deber�a ser el 2do elemento", (Integer) 8, numeros.getElement());

		//CASE 2: the last
		setUp();
		numeros.add(9);
		numeros.deleteAtK(1);
		assertEquals("No se elimin� correctamente el ultimo elemento", (Integer) 8, numeros.getElementAtK(1));

		//CASE 3: only one elem
		setUp2();
		numeros.deleteAtK(0);
		try{
			assertEquals("El primer elemento deber�a ser null", null, numeros.getFirst());
			assertEquals("El ultimo elemento deber�a ser null", null, numeros.getLast());
			assertEquals("El elemento actual deber�a ser null", null, numeros.getElement());
		}
		catch(Exception e){

		}
		//CASE 4: not empty list, elem on the size range
		setUp();
		numeros.deleteAtK(4);
	}
	public void testIterator()
	{
		setUp();
		Iterator<Integer> iter = numeros.iterator();
		iter.next();
		assertEquals("Deberia de haber avanzado", (Integer) 8, numeros.getElement());
	}
	public void testDelete()
	{
		setUp();
		try {
			numeros.delete();
			assertEquals((Integer) 8 , numeros.getElement());
			
			numeros.next();
			 
			numeros.delete();
			assertEquals((Integer) 6 , numeros.getElement());
			
			numeros.next();
			numeros.next();
			numeros.next();
			numeros.next();
			
			numeros.delete();
			
			assertNull(numeros.getCurrent().getNext());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		setUp2();
		try {
			numeros.delete();
			assertNull(numeros.getElement());
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			numeros.delete();
			fail();
		} catch (Exception e) {
			// TODO Auto-generated catch block
		}
	}
}
